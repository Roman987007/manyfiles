import java.io.*;

/**
 * Класс {@code SelectionSixDigitNumber} содержит алгоритм отбора шестизначных чисел из файла intdata.dat и записывает их в файл int6data.dat.
 * @author Roman Petrushov
 * @data 01.11.2016
 */
public class SelectionSixDigitNumber {
    public static void main(String[] args) throws IOException {

       BufferedReader intData = new BufferedReader(new FileReader("intdata.dat"));
       BufferedWriter out6data = new BufferedWriter(new FileWriter("int6data.dat"));

        String str;

        /**
         * Условие отбирает шестизначные числа из файла intdata.dat и записывает в файл int6data.dat
         */
        while ((str = intData.readLine())!= null){
            if(str.length() == 6) {
                out6data.write(str + "\r\n");
            }
            if (str.length() == 5){
                out6data.write(0+ str +"\r\n");
            }
            if (str.length() == 4){
                out6data.write(00+ str +"\r\n");
            }
        }
        intData.close();
        out6data.close();


    }
}
