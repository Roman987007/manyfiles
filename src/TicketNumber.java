import java.io.*;

/**
 * Класс {@code TicketNumber} содержит алгоритм определения счастливого билета и после записывает счастливые номера в файл txt6data.dat.
 * @author Roman Petrushov
 * @data 01.11.2016
 */
public class TicketNumber {
    public static void main(String[] args) throws IOException {
        BufferedReader int6data = new BufferedReader(new FileReader("int6data.dat"));
        BufferedWriter txt6data = new BufferedWriter(new FileWriter("txt6data.dat"));

        String str = int6data.readLine();
        while (str != null){
            if(HappyTicket(str)){
                txt6data.write(str + "\r\n");
            }
            str = int6data.readLine();
        }

        int6data.close();
        txt6data.close();
        }

    /**
     * Возвращает {@code true}, если число счастливое
     * (сумма левых 3-х цифр равна сумме правых 3-х цифр),
     * или {@code false}, если нет
     *
     * @param str - строка, содержащая шестизначное число
     * @return - {@code true}, если число счастливое, иначе {@code false}
     */
        public static boolean HappyTicket(String str){
            int sumFistNumber = 0;
            int sumSecondNumber = 0;
            int number = Integer.parseInt(str);

            for (int i = 0; i < 3; i++) {
                sumFistNumber += number % 10;
                number /= 10;
            }

            for (int i = 0; i < 3; i++) {
                sumSecondNumber += number % 10;
                number /= 10;
            }
            if (sumFistNumber == sumSecondNumber) {
                return true;
            }
            else {
                return false;
            }

        }
    }
