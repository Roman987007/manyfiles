import java.io.*;
import java.util.Random;

/**
 * Класс {@code InputDataFile} содержит алгоритм заполнение файла intdata.dat рандомными числами.
 * @author Roman Petrushov
 * @data 01.11.2016
 */
public class InputDataFile {
    public static void main(String[] args) throws IOException {
        Random rand = new Random();

        BufferedWriter outdata = new BufferedWriter(new FileWriter("intdata.dat"));

        int length = 6 + rand.nextInt(100);
        int []ticketNimber = new int[length];

        /**
         * Алгоритм заполняет массив ticketNimber, рандомными числами и записывает их файл intdata.dat.
         */
        for (int i = 0; i < ticketNimber.length; i++){
            ticketNimber[i] = 10 + rand.nextInt(999999);
            outdata.write(ticketNimber[i] + "\r\n");
        }
        outdata.close();
    }
}
